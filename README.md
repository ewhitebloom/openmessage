Eric Whitebloom's Solution to Backend Challenge

To run you need to install Docker.

Once Docker has been installed run:

`docker-compose up --build`

then the app should be running on port 3000.

If you want to run the test suite, you'll need to get a shell into the app container and run

`npm run test`.

In order to get a shell, find the container id with `docker ps` then run `docker exec -it <container id> bash`.

Let me know if you have questions!