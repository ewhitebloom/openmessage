class User {
  constructor(id, name, email, username) {
    this.id = id;
    this.name = name;
    this.email = email;
    this.username = username;
  }


  validate() {
    let errors = []

    if (!this.id || (typeof this.id != 'number')) {
      errors.push('Id is missing or of wrong type (number)')
    }

    if (!this.name || !this.name.length) {
      errors.push('Name is missing.')
    }

    if (!this.email || !this.email.length) {
      errors.push('Email is missing.')
    }

    if (!this.username || !this.username.length) {
      errors.push('Username is missing.')
    }

    return errors;
  }

  createSQL() {
    return `INSERT INTO users (id, name, email, username) VALUES (${this.id}, '${this.name}', '${this.email}', '${this.username}');`
  }
}

module.exports = User;
