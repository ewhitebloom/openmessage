const { query } = require('../db')
const { parse } = require('querystring');
const User = require('./models/user')
const urlMod = require('url');

const getAllUsers = async function (req, res) {
  let url = req.url;

  const queryParams = urlMod.parse(url, true).query

  let users;
  const props = Object.keys(queryParams);

  if (props.length) {
    if (!_checkValidUserProps(queryParams)) {
      res.writeHead(400)
      return res.end()
    }

    let sqlQuery = `SELECT * FROM users WHERE `

    let constraints = []

    for (const prop in queryParams) {
      constraints.push(`${prop} = '${queryParams[prop]}'`)
    }

    sqlQuery += constraints.join(', ')
    sqlQuery += ';'

    try {
      users = await query(sqlQuery);
    } catch (e) {
      console.error(e)
      console.error(e.stack)
      res.writeHead(500)
      return res.end("There's been an internal error. Please try again.")
    }
  } else {
    try {
      users = await query(`SELECT * FROM users;`);
    } catch (e) {
      console.error(e)
      console.error(e.stack)
      res.writeHead(500)
      return res.end("There's been an internal error. Please try again.")
    }
  }

  res.write(JSON.stringify(users.rows));
  res.end();
}

const getUser = async function (req, res) {
  let url = req.url;
  const userId = url.match(/users\/(\d+)\/?/)[1]

  let results
  try {
    results = await query(`SELECT * FROM users WHERE id = ${userId}`)
  } catch (e) {
    console.error(e)
    console.error(e.stack)
    res.writeHead(400)
    return res.end("There's been a problem processing your request. Please use a valid user id.")
  }


  res.write(JSON.stringify(results.rows));
  res.end();
}

const getUserAddress = async function (req, res) {
  let url = req.url;
  const userId = url.match(/users\/(\d+)\/?/)[1]

  let results
  try {
    results = await query(`SELECT addresses.* FROM users JOIN addresses ON addresses.user_id = users.id WHERE id = ${userId};`)
  } catch (e) {
    console.error(e)
    console.error(e.stack)
    res.writeHead(400)
    return res.end("There's been a problem processing your request. Please use a valid user id.")
  }


  res.write(JSON.stringify(results.rows));
  res.end();
}

const getUserTodos = async function (req, res) {
  let url = req.url;
  const userId = url.match(/users\/(\d+)\/?/)[1]

  let results
  try {
    results = await query(`SELECT todos.* FROM users JOIN todos ON todos.user_id = users.id WHERE users.id = ${userId};`)
  } catch (e) {
    console.error(e)
    console.error(e.stack)
    res.writeHead(400)
    return res.end("There's been a problem processing your request. Please use a valid user id.")
  }

  res.write(JSON.stringify(results.rows));
  res.end();
}

const handleNoRoute = async function (req, res) {
  res.writeHead(404);
  res.end()
}

const createUser = async function (req, res) {
  let body = '';

  req.on('data', chunk => {
    body += chunk.toString(); // convert Buffer to string
  });

  req.on('end', async () => {
    let parsed
    
    try {
      parsed = JSON.parse(body)
    } catch (e) {
      console.error(e)
      console.error(e.stack)
      res.writeHead(400)
      return res.end('malformed JSON.')
    }

    let user = new User(parsed.id, parsed.name, parsed.email, parsed.username);

    let errors = user.validate();
    if (errors.length) {
      res.writeHead(400)
      return res.end(errors.join(', '))
    }

    try {
      await query(user.createSQL())
    } catch (e) {
      console.error(e)
      console.error(e.stack)
      res.writeHead(500)
      return res.end("There was an internal error. Try again later.")
    }
  
    res.end('ok');
  });
}

const _checkValidUserProps = function (input) {
  const validProps = ['name', 'username', 'email']

  for (const prop in input) {
    if (!validProps.includes(prop.toLowerCase())) {
      return false
    }
  }

  return true
}

module.exports = {
  getAllUsers,
  getUser,
  getUserAddress,
  getUserTodos,
  handleNoRoute,
  createUser
};
