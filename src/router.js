const {
  getAllUsers,
  getUser,
  getUserAddress,
  getUserTodos,
  handleNoRoute,
  createUser
} = require('./controller')

const router = async function (req, res) {
  let url = req.url;

  if (req.method === 'POST' && /users\/?/.test(url)) {
    return createUser(req, res);
  } else if (req.method === 'POST') {
    return handleNoRoute(req, res);
  }

  if (/^\/users(?:\/?|\?(\w+)=(\w+))$/.test(url)) {
    getAllUsers(req, res);
  } else if (/^\/users\/\d+\/?$/.test(url)) {
    getUser(req, res)
  } else if (/^\/users\/\d+\/address$/.test(url)) {
    getUserAddress(req, res);
  } else if (/^\/users\/\d+\/todos$/.test(url)) {
    getUserTodos(req, res);
  } else {
    handleNoRoute(req, res);
  }
}

module.exports = router;
