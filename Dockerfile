FROM node:10
RUN mkdir /openMessage
ADD . /openMessage
WORKDIR /openMessage
RUN npm i
EXPOSE 3000
CMD ["npm", "start"]