process.env.NODE_ENV = 'test';

const assert = require('assert');
const fetch = require('node-fetch')
const server = require('../app')

let chai = require('chai');
let chaiHttp = require('chai-http');
let should = chai.should();

chai.use(chaiHttp);

describe('Controller', function () {
  describe('#getAllUsers()', function () {
    it('should return all users', function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server).get('/users')
        .end((err, res) => {
          res.should.have.status(200);
          const json = JSON.parse(res.text)
          json.should.be.a('array');
          json.length.should.be.eql(10);
          done();
        })
    });

    it('should return filtered users', function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server).get('/users?username=Bret')
        .end((err, res) => {
          res.should.have.status(200);
          const json = JSON.parse(res.text)
          json.should.be.a('array');
          json.length.should.be.eql(1);
          done();
        })
    });
  });

  describe('#getUser', function () {
    it('should return the right user', function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server).get('/users/1')
        .end((err, res) => {
          res.should.have.status(200);
          const json = JSON.parse(res.text)
          json.should.be.a('array');
          json.length.should.be.eql(1);
          json[0].name.should.be.eq('Leanne Graham')
          done();
        })
    })
  })

  describe('#getUserAddress', function () {
    it('should return the right user address', function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server).get('/users/1/address')
        .end((err, res) => {
          res.should.have.status(200);
          const json = JSON.parse(res.text)
          json.should.be.a('array');
          json.length.should.be.eql(1);
          json[0].street.should.be.eq('Kulas Light')
          json[0].suite.should.be.eq('Apt. 556')
          json[0].city.should.be.eq('Gwenborough')
          json[0].zipcode.should.be.eq('92998-3874')
          done();
        })
    })
  })

  describe('#getUserTodos', function () {
    it("should return the right user's todos", function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server).get('/users/1/todos')
        .end((err, res) => {
          res.should.have.status(200);
          const json = JSON.parse(res.text)
          json.should.be.a('array');
          json.length.should.be.eql(20);
          json[0].title.should.be.eql('quis ut nam facilis et officia qui')
          done();
        })
    })
  })

  describe('#createUser', function () {
    it("should not create a User when missing fields", function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server)
           .post('/users')
           .send(JSON.stringify({name: "Bob"}))
           .end((err, res) => {
             res.should.have.status(400);
             done();
           })
    })

    it("should create a User with accurate fields", function (done) {
      process.env.NODE_ENV = 'test'

      chai.request(server)
        .post('/users')
        .send(JSON.stringify({
          id: 234,
          name: "Bob",
          username: "Jones",
          email: "Bob@jones.com"
        }))
        .end((err, res) => {
          res.should.have.status(200);
          done();
        })
    })
  })
});