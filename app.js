const { importAllData } = require('./db/seed/importData')
const router = require('./src/router')
const http = require('http');

let port;

const server =  http.createServer(async function (req, res) {
    port = process.env.NODE_ENV === 'test' ? 3010 : 3000

    router(req, res);
  }).listen(port, async function () {
    console.log("server listening on port 3000");
  });

module.exports = server;
