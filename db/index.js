const { Client } = require('pg')

const client = new Client({
  host: 'db',
  port: 5432,
  user: 'postgres',
  password: 'password',
  database: 'postgres'
})


client.connect()
  .then(() => console.log('connected'))
  .catch(err => console.error('connection error', err.stack))


module.exports = {
  query: (text) => {
    return client.query(text)
  }
}