const { importAllData } = require('./importData')

console.log('Seeding data....');

(async () => {
  await importAllData()
})()

console.log('Done seeding.');