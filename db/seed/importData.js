const fetch = require('node-fetch');
const { query } = require('../')

const importUsers = async function () {
  const response = await fetch('https://jsonplaceholder.typicode.com/users/');

  if (!response.ok) {
    throw new Error('Problem fetching user data.')
  }

  const jsonData = await response.json();

  await query(`DROP TABLE IF EXISTS users, addresses, todos; 
    CREATE TABLE users (
      ID INT PRIMARY KEY NOT NULL,
      NAME           VARCHAR(50) NOT NULL,
      USERNAME       VARCHAR(50) NOT NULL,
      EMAIL          VARCHAR(50) NOT NULL
  );`)

  await query(`CREATE TABLE addresses (
    user_id int REFERENCES users(id),
    street  VARCHAR(50) NOT NULL,
    suite VARCHAR(50),
    city VARCHAR(150) NOT NULL,
    zipcode VARCHAR(30) NOT NULL,
    lat REAL NOT NULL,
    lng REAL NOT NULL
  );`)

  const insertUsersSQL = 'INSERT INTO users (id, name, username, email) VALUES '
  const insertAddressesSQL = 'INSERT INTO addresses (user_id, street, suite, city, zipcode, lat, lng) VALUES '

  let userRows = [],
    addressRows = []

  for (const user of jsonData) {
    userRows.push(`(${user.id}, '${user.name}', '${user.username}', '${user.email}')`)
    addressRows.push(`(${user.id}, '${user.address.street}', '${user.address.suite}', '${user.address.city}', '${user.address.zipcode}', ${Number(user.address.geo.lat)}, ${Number(user.address.geo.lng)})`)
  }

  await query(insertUsersSQL + userRows.join(',\n') + ';')
  await query(insertAddressesSQL + addressRows.join(',\n') + ';')
}

const importTodos = async function () {
  const response = await fetch('https://jsonplaceholder.typicode.com/todos');

  if (!response.ok) {
    throw new Error('Problem fetching todos data.')
  }

  const jsonData = await response.json();

  await query(`DROP TABLE IF EXISTS todos; CREATE TABLE todos (
    ID INT PRIMARY KEY NOT NULL,
    user_id int REFERENCES users(id),
    title TEXT NOT NULL,
    completed BOOLEAN 
   );`)

  const insertSQL = 'INSERT INTO todos (id, user_id, title, completed) VALUES '

  let rows = []

  for (const todo of jsonData) {
    rows.push(`(${todo.id}, ${todo.userId}, '${todo.title}', ${todo.completed})`)
  }

  await query(insertSQL + rows.join(',\n') + ';')
}

const categorizeTodoItemLengths = async function () {
  await query(`ALTER TABLE todos
   ADD COLUMN lengthCategory VARCHAR(20),
   ADD CONSTRAINT check_types 
   CHECK (lengthCategory IN ('short', 'medium', 'long'));`)
  await query(`UPDATE todos SET lengthCategory = 'short' WHERE LENGTH(title) <= 25;`);
  await query(`UPDATE todos SET lengthCategory = 'medium' WHERE LENGTH(title) > 25 AND LENGTH(title) <=50;`);
  await query(`UPDATE todos SET lengthCategory = 'medium' WHERE LENGTH(title) > 50`);
}

const importAllData = async function () {
  await importUsers();
  await importTodos();
  await categorizeTodoItemLengths();
}


module.exports = {
  importUsers,
  importAllData
}